import { Pipe, PipeTransform } from '@angular/core';
import { UtilsService } from '../services/utils.service';

@Pipe({
  name: 'romanize'
})
export class RomanizePipe implements PipeTransform {

  constructor(
    private utilsService: UtilsService
  ) {

  }

  transform(value: any): any {
    return this.utilsService.romanize(value) || value;
  }

}
