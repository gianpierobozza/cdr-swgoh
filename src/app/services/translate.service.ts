import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {
  data: any = {};

  constructor(
    private http: HttpClient
  ) { }

  public translate(key: any): any {
    return this.data[key] || key;
  }

  public use(lang: string): Promise<{}> {
    return new Promise<{}>((resolve, reject) => {
      const userLang = lang || 'en_GB';
      const langPath = 'assets/i18n/' + userLang + '.json';
      localStorage.setItem('userLang', userLang);

      this.http.get<{}>(langPath).subscribe(
        translation => {
          this.data = Object.assign({}, translation || {});
          resolve(this.data);
        },
        error => {
          this.data = {};
          resolve(this.data);
        }
      );
    });
  }
}
