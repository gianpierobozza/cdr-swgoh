import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(
    private http: HttpClient
  ) { }

  /* AUTH endpoints */
  public sendAuthTokenToUser(tokenPackage) {
    return this.http.post(environment.apiServerUrl + 'sendAuthTokenToUser', tokenPackage).pipe(catchError(this.handleError));
  }

  public verifyAuthToken(token) {
    const body = {
      token: token
    };
    return this.http.post(environment.apiServerUrl + 'verifyAuthToken', body).pipe(catchError(this.handleError));
  }

  public verifySessionId() {
    const body = {
      authUser: localStorage.getItem('authUser'),
      sessionId: localStorage.getItem('sessionId')
    };
    console.log('verifySessionId body',body)
    return this.http.post(environment.apiServerUrl + 'verifySessionId', body).pipe(catchError(this.handleError)).toPromise();
  }

  /* SQUADS endpoints */

  public getPlayerSquads() {
    return this.http.get(environment.apiServerUrl + 'getPlayerSquads?discordUser=' + encodeURIComponent(localStorage.getItem('authUser'))).pipe(catchError(this.handleError));
  }

  public saveSquad(body) {
    return this.http.post(environment.apiServerUrl + 'saveSquad', body).pipe(catchError(this.handleError));
  }

  public removeSquad(id) {
    return this.http.get(environment.apiServerUrl + 'removeSquad?id=' + id).pipe(catchError(this.handleError));
  }

  /* MISC */

  public getPlayerRoster() {
    return this.http.get(environment.apiServerUrl + 'getPlayerRoster?discordUser=' + encodeURIComponent(localStorage.getItem('authUser'))).pipe(catchError(this.handleError));
  }

  public pingServer() {
    return this.http.get(environment.apiServerUrl + 'ping').pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    console.log(error)
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(error.error);
  };
}
