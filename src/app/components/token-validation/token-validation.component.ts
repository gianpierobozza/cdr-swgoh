import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from '../../services/api-service.service';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-token-validation',
  templateUrl: './token-validation.component.html',
  styleUrls: ['./token-validation.component.scss']
})
export class TokenValidationComponent implements OnInit, OnDestroy {

  private token: string;
  private verificationSub;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apiServiceService: ApiServiceService
  ) {
    this.activatedRoute.params.subscribe( params => {
      this.token = params['token'];
    }); 
  }

  ngOnInit() {
    this.verificationSub = this.apiServiceService.verifyAuthToken(this.token).subscribe((result: any) => {
      localStorage.setItem('authUser', result.authUser);
      localStorage.setItem('sessionId', result.sessionId);
      this.router.navigate([result.action]);
    }, (error) => {
      this.router.navigate([error.action]);
    });
  }

  ngOnDestroy() {
    this.verificationSub.unsubscribe();
  }

}
