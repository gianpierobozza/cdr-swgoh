import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../../services/translate.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() { }

  public setLang(lang: string) {
    this.translate.use(lang);
  }

}
