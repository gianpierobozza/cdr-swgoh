import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ApiServiceService } from '../../services/api-service.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, OnDestroy {

  public authForm: FormGroup = new FormGroup({
    playerName: new FormControl('', Validators.required),
    selectedAction: new FormControl('', Validators.required)
  });

  public actions: any = [
    { value: 'squads', label: 'AUTH_COMPONENT_RADIO_GROUP_SQUADS' },
    { value: 'admin', label: 'AUTH_COMPONENT_RADIO_GROUP_ADMIN' }
  ];

  private pingSub;

  constructor(
    private apiServiceService: ApiServiceService
  ) {
    this.pingSub = this.apiServiceService.pingServer().subscribe((result) => {
      console.log(result);
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.pingSub.unsubscribe();
  }

  sendAuthTokenToUser({ value, valid }) {
    console.log(value, valid);
    if (valid) {
      this.apiServiceService.sendAuthTokenToUser(value).subscribe((result) => {
        console.log(result);
      });
    }
  }

}
