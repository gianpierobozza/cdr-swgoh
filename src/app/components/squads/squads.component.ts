import { Component, Inject, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatAutocomplete } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ApiServiceService } from '../../services/api-service.service';
import { UtilsService } from 'src/app/services/utils.service';
import { TranslateService } from 'src/app/services/translate.service';

export interface Unit {
  defId: string;
  nameKey: string;
  level: number;
  gear: string;
  zetas: number;
  gp: number;
}

export interface DialogData {
  squadId: string;
  squadName: string;
}

@Component({
  selector: 'app-squads',
  templateUrl: './squads.component.html',
  styleUrls: ['./squads.component.scss']
})
export class SquadsComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('leader') public leader: MatAutocomplete;
  @ViewChild('memberA') public memberA: MatAutocomplete;
  @ViewChild('memberB') public memberB: MatAutocomplete;
  @ViewChild('memberC') public memberC: MatAutocomplete;
  @ViewChild('memberD') public memberD: MatAutocomplete;
  @ViewChild('saveBtn') public saveBtn: HTMLButtonElement;

  public playerRoster = null;
  public squadsLoading: boolean = true;
  public addSquadTooltip: string = 'SQUADS_COMPONENT_ADD_SQUAD_DISABLED_TOOTLIP';
  public addSquadDisabled: boolean = true;
  public saveSquadDisabled: boolean = true;
  public addSquadTotalGP: number = 0;
  public leaderOptions: Observable<Unit[]>;
  public memberAOptions: Observable<Unit[]>;
  public memberBOptions: Observable<Unit[]>;
  public memberCOptions: Observable<Unit[]>;
  public memberDOptions: Observable<Unit[]>;

  public saveSquadForm: FormGroup = new FormGroup({
    squadName: new FormControl('', Validators.required),
    squadLeader: new FormControl({value: '', disabled: true}, Validators.required),
    squadMemberA: new FormControl({value: '', disabled: true}, Validators.required),
    squadMemberB: new FormControl({value: '', disabled: true}, Validators.required),
    squadMemberC: new FormControl({value: '', disabled: true}, Validators.required),
    squadMemberD: new FormControl({value: '', disabled: true}, Validators.required),
    id: new FormControl('')
  });;

  private squadsData;
  private squadsDataSub;
  private onAddSquadFormChangesSub;
  private leaderAutocompleteOpenSub = null;
  private memberAAutocompleteOpenSub = null;
  private memberBAutocompleteOpenSub = null;
  private memberCAutocompleteOpenSub = null;
  private memberDAutocompleteOpenSub = null;
  private formControlNames = ['squadLeader', 'squadMemberA', 'squadMemberB', 'squadMemberC', 'squadMemberD'];
  private options: Unit[] = [];

  constructor(
    public dialog: MatDialog,
    private apiServiceService: ApiServiceService,
    private translateService: TranslateService,
    private utilsService: UtilsService
  ) {
    this.leaderOptions = this.saveSquadForm.get('squadLeader').valueChanges
      .pipe(
        startWith<string | Unit>(''),
        map(value => typeof value === 'string' ? value : value.defId),
        map(defId => defId ? this._filter(defId) : this.filterFormSelectedValues(this.options.slice()))
      );
    this.memberAOptions = this.saveSquadForm.get('squadMemberA').valueChanges
      .pipe(
        startWith<string | Unit>(''),
        map(value => typeof value === 'string' ? value : value.defId),
        map(defId => defId ? this._filter(defId) : this.filterFormSelectedValues(this.options.slice()))
      );
    this.memberBOptions = this.saveSquadForm.get('squadMemberB').valueChanges
      .pipe(
        startWith<string | Unit>(''),
        map(value => typeof value === 'string' ? value : value.defId),
        map(defId => defId ? this._filter(defId) : this.filterFormSelectedValues(this.options.slice()))
      );
    this.memberCOptions = this.saveSquadForm.get('squadMemberC').valueChanges
      .pipe(
        startWith<string | Unit>(''),
        map(value => typeof value === 'string' ? value : value.defId),
        map(defId => defId ? this._filter(defId) : this.filterFormSelectedValues(this.options.slice()))
      );
    this.memberDOptions = this.saveSquadForm.get('squadMemberD').valueChanges
      .pipe(
        startWith<string | Unit>(''),
        map(value => typeof value === 'string' ? value : value.defId),
        map(defId => defId ? this._filter(defId) : this.filterFormSelectedValues(this.options.slice()))
      );
  }

  async ngOnInit() {
    this.squadsDataSub = await this.getPlayerSquads();
    this.onAddSquadFormChanges();
  }

  ngAfterViewInit() {
    this.leaderAutocompleteOpenSub = this.leader.opened.subscribe(() => {
      this.saveSquadForm.get('squadLeader').setValue('', {emitEvent: true, onlySelf: true});
    });

    this.memberAAutocompleteOpenSub = this.memberA.opened.subscribe(() => {
      this.saveSquadForm.get('squadMemberA').setValue('', {emitEvent: true, onlySelf: true});
    });

    this.memberBAutocompleteOpenSub = this.memberB.opened.subscribe(() => {
      this.saveSquadForm.get('squadMemberB').setValue('', {emitEvent: true, onlySelf: true});
    });

    this.memberCAutocompleteOpenSub = this.memberC.opened.subscribe(() => {
      this.saveSquadForm.get('squadMemberC').setValue('', {emitEvent: true, onlySelf: true});
    });

    this.memberDAutocompleteOpenSub = this.memberD.opened.subscribe(() => {
      this.saveSquadForm.get('squadMemberD').setValue('', {emitEvent: true, onlySelf: true});
    });
  }

  ngOnDestroy() {
    this.squadsDataSub.unsubscribe();
    this.onAddSquadFormChangesSub.unsubscribe();
    this.leaderAutocompleteOpenSub.unsubscribe();
    this.memberAAutocompleteOpenSub.unsubscribe();
    this.memberBAutocompleteOpenSub.unsubscribe();
    this.memberCAutocompleteOpenSub.unsubscribe();
    this.memberDAutocompleteOpenSub.unsubscribe();
  }

  public onSaveSquad() {
    let formValue = this.saveSquadForm.value;
    this.saveSquadForm.disable();
    this.saveBtn.disabled = true;
    this.squadsLoading = true;
    this.addSquadTotalGP = 0;
    formValue['discordUser'] = localStorage.getItem('authUser');

    this.apiServiceService.saveSquad(formValue).subscribe((result) => {
      console.log(result);
      if (result && result.hasOwnProperty('message')) {
        this.saveSquadForm.reset({
          squadName: '',
          squadLeader: '',
          squadMemberA: '',
          squadMemberB: '',
          squadMemberC: '',
          squadMemberD: '',
          id: ''
        }, { emitEvent: true });
        this.getPlayerSquads();
        this.saveBtn.disabled = !this.saveSquadForm.valid;
      }
    });
  }

  public editSquad(squadId, squadName, squadMembers) {
    this.saveSquadForm.get('squadName').setValue(squadName);
    this.saveSquadForm.get('id').setValue(squadId);
    squadMembers.forEach((member, key) => {
      this.saveSquadForm.get(this.formControlNames[key]).setValue(member[0]);
    });
  }

  public cancelAddingSquad() {
    this.addSquadTotalGP = 0;
    this.saveSquadForm.reset({
      squadName: '',
      squadLeader: '',
      squadMemberA: '',
      squadMemberB: '',
      squadMemberC: '',
      squadMemberD: '',
      id: ''
    }, { emitEvent: true });
  }

  public deleteSquadOpenConfirm(squadId, squadName) {
    const dialogRef = this.dialog.open(DeleteSquadDialog, {
      width: '400px',
      data: {
        squadId: squadId,
        squadName: squadName
      }
    });

    dialogRef.afterClosed().subscribe(id => {
      if (id !== undefined) {
        this.squadsLoading = true;
        this.saveSquadForm.disable();
        this.apiServiceService.removeSquad(id).subscribe(result => {
          this.getPlayerSquads();
        });
      }
    });
  }

  public displayFn(unit?: Unit): string | undefined {
    return unit ? this.translateService.translate(unit.defId) : undefined;
  }

  private _filter(name: string): Unit[] {
    const filterValue = name.toLowerCase();
    return this.options.filter(option => {
      let value;
      let used = this.getFormSelectedValues();
      if (this.translateService.translate(option.defId).toLowerCase().indexOf(filterValue) !== -1 && !used.includes(option.defId)) {
        value = this.translateService.translate(option.defId).toLowerCase();
      }
      return value;
    });
  }

  private onAddSquadFormChanges() {
    this. onAddSquadFormChangesSub = this.saveSquadForm.valueChanges.subscribe(formValue => {
      this.addSquadTotalGP = 0;
      Object.keys(formValue).forEach(key => {
        if (formValue[key] !== null &&
            formValue[key] !== "" &&
            formValue[key].hasOwnProperty('gp'))
          this.addSquadTotalGP += formValue[key].gp;
      });
    });
  }

  private getPlayerSquads() {
    this.apiServiceService.getPlayerSquads().subscribe((playerSquads) => {
      console.log(playerSquads);
      if (playerSquads) {
        this.apiServiceService.getPlayerRoster().subscribe((playerRoster) => {
          if (playerRoster) {
            playerRoster = this.removeUsedUnits(playerRoster, playerSquads['squadsInfo']);
            this.playerRoster = playerRoster;
            this.addSquadDisabled = false;
            this.addSquadTooltip = 'SQUADS_COMPONENT_ADD_SQUAD_ENABLED_TOOTLIP';
            this.optionsForAutocomplete();
            this.saveSquadForm.enable();
            this.squadsData = playerSquads;
          }
        });
      }
    });
  }

  private filterFormSelectedValues(options: Unit[]): Unit[] {
    let used = this.getFormSelectedValues();
    return options.filter(option => !used.includes(option.defId));
  }

  private getFormSelectedValues() {
    let used = [];
    Object.keys(this.saveSquadForm.controls).forEach(key => {
      if (this.saveSquadForm.get(key).value !== null &&
          this.saveSquadForm.get(key).value !== "" &&
          this.saveSquadForm.get(key).value.hasOwnProperty('defId'))
        used.push(this.saveSquadForm.get(key).value.defId);
    });
    return used;
  }

  private optionsForAutocomplete() {
    this.options = [];
          
    console.log('player roster', this.playerRoster);
    
    this.playerRoster.forEach(unit => {
      if (unit.combatType === "CHARACTER") {
        let zetas = 0;
        unit.skills.map(skill => skill.isZeta && skill.tier == 8 ? zetas++ : zetas);
        this.options.push({
          defId: unit.defId,
          nameKey: unit.nameKey,
          level: unit.level,
          gear: this.utilsService.romanize(unit.gear),
          zetas: zetas,
          gp: unit.gp
        });
      }
    });
    this.squadsLoading = false;
  }

  private removeUsedUnits(playerRoster, squadsInfo) {
    squadsInfo[0].forEach(squad => {
      squad.members.forEach(member => {
        playerRoster.forEach((rosterElement, key) => {
          if (member === rosterElement.defId)
            delete playerRoster[key];
        });
      });
    });
    return playerRoster;
  }

}

@Component({
  selector: 'delete-squad-dialog',
  templateUrl: 'delete-squad-dialog.html',
})
export class DeleteSquadDialog {

  constructor(
    public dialogRef: MatDialogRef<DeleteSquadDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {

  }

  public onOkClick() {
    console.log('delete', this.data.squadId);
    this.dialogRef.close(this.data.squadId);
  }

  public onNoClick() {
    this.dialogRef.close();
  }

}